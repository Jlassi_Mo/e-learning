﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace projetFinal.Models
{
    public class ClsCours
    {
        public string Id { get; set; }
        public string Nomcours { get; set; }
        public string Description { get; set; }
        public string Nbremodule { get; set; }
        public string Auteur { get; set; }
        public float Prix { get; set; }
        public string Image { get; set; }
        public string Niveau { get; set; }
        public string Categorie { get; set; }
        public string Notescours { get; set; }
        public string Solutionnaires { get; set; }
        public string Laboratoires { get; set; }
        public string Exercices { get; set; }
        public string Quiz { get; set; }
        


    }
}