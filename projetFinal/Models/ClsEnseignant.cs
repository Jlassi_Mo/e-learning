﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace projetFinal.Models
{
    public class ClsEnseignant
    {
        public int Id { get; set; }
        public string Prenom { get; set; }
        public string Nom { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}