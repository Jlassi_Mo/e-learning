﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace projetFinal.Models
{
    public class ClsEtudiant
    {


        public int IdEtudiant { get; set; }
        public string Prenom { get; set; }
        public string Nom { get; set; }
        public string Email { get; set; }
        public string Niveau { get; set; }
        public string LoginEtudiant { get; set; }
        public string Password { get; set; }
        public int Status { get; set; }
    }
}