﻿using projetFinal.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace projetFinal.Controllers
{
    public class EnseignantController : Controller
    {
        // GET: Enseignant
        public ActionResult Index()
        {
            return View();
        }
   
       // insert new course
        [HttpPost]
        public ActionResult InsertCours(string Nom, string Desc, string Prix,string Cat ,string Niveau)
        {
            DataLinkDataContext db = new DataLinkDataContext();

            cours tmp = new cours
            {
                nomcours = Nom,
                description = Desc,
                prix = double.Parse(Prix),
                nomcat = Cat,
                niveau = Niveau,
                auteur = int.Parse(Session["IdEnseignant"].ToString())
                
            };
            db.cours.InsertOnSubmit(tmp);
            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
              
            }

   


            return Json("");
        }
        [HttpPost]
        public ActionResult UpdateCours(string Nom, string Desc, string Prix,string idCours)
        {
            DataLinkDataContext db = new DataLinkDataContext();

            var query =
            from course in db.cours
            where course.Idcours == int.Parse(idCours)
            select course;
            foreach (cours course in query)
            {
                if (Nom != "")
                {
                    course.nomcours = Nom;
                }
            }
            foreach (cours course in query)
            { 
                if (Desc != "")
                {
                    course.description = Desc;
                }
            }
            foreach (cours course in query)
            { 
                if (Prix != "")
                {
                    course.prix = double.Parse(Prix);
                }
            }

            // Submit the changes to the database.
            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                // Provide for exceptions.
            }
            return Json("");
        }
        public ActionResult UploadFile(string IdCours,string Name,string ext)
        {
            ViewBag.FullName = Name + IdCours+ext;
            ViewBag.idCours = IdCours;
            return View();
        }
        [HttpPost]
        public ActionResult UploadFile(string fullName,string idcours)
        {
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var path = Path.Combine(Server.MapPath("~/Upload/"), fullName);
                    file.SaveAs(path);
                }
            }
            ViewBag.liens = getListChapitre(int.Parse(idcours));
            DataLinkDataContext db = new DataLinkDataContext();

            var query =
            from course in db.cours
            where course.Idcours == int.Parse(idcours)
            select course;

            // Execute the query, and change the column values
            // you want to change cours table
                if (fullName.Contains("Notes_"))
                {
                    foreach (cours course in query)
                    {
                        course.notes_cours = fullName;

                    }
                }
                else if(fullName.Contains("Solutions_"))
                {
                    foreach (cours course in query)
                    {
                        course.solutionnaire = fullName;

                    }
                }
                else if (fullName.Contains("Labs_"))
                {
                    foreach (cours course in query)
                    {
                        course.laboratoires = fullName;

                    }
                }
                else if (fullName.Contains("Exercies_"))
                {
                    foreach (cours course in query)
                    {
                        course.exercices = fullName;

                    }
                }
                else if (fullName.Contains("Quiz_"))
                {
                    foreach (cours course in query)
                    {
                        course.quiz = fullName;

                    }
                }
          
         else if (fullName.Contains("Photo_"))
            {
                foreach (cours course in query)
                {
                    course.image = fullName;

                }
            }
            //you want to change chapite table
            else if (fullName.Contains("Video_"))
            {

                chapitre tmp = new chapitre
                {
                    lien = fullName,
                   id_cours = int.Parse(idcours)
                };
                db.chapitre.InsertOnSubmit(tmp);


                try
                {
                    db.SubmitChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.viol_constraint = "your are already enrolled,";

                    return RedirectToAction("ShowDetailCours", new { id = idcours });
                }

                return RedirectToAction("ShowDetailCours", new { id = idcours });


            }
            // Submit the changes to the database.
            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                // Provide for exceptions.
            }

            return RedirectToAction("ShowDetailCours", new { id = idcours });

        }
        public ActionResult TeacherCorner()
        {
            DataLinkDataContext db = new DataLinkDataContext();
            cours s1 = new cours();
            var requet = from element in db.niveauscolaire
                         select element.niveau;


            ViewBag.niveau = requet;

            var Query = from element in db.categorie
                        select element.nomcat;

            ViewBag.cat = Query;

            var resultat = (from m in db.cours
                            where m.auteur == int.Parse(Session["IdEnseignant"].ToString())
                            select new ClsCours { Id = (m.Idcours).ToString(), Nomcours = m.nomcours, Description = m.description, Nbremodule = m.nbremodule, Prix = (float)m.prix, Image = m.image, Niveau = m.niveau, Categorie = m.nomcat }).ToList();

            return View(resultat);
        }
        public ActionResult ShowDetailCours(string id)
        {
            DataLinkDataContext db = new DataLinkDataContext();
            cours s1 = new cours();
            var resultat = (from m in db.cours
                            where m.Idcours == Int32.Parse(id)
                            select new ClsCours { Id = (m.Idcours).ToString(), Nomcours = m.nomcours, Description = m.description, Nbremodule = m.nbremodule, Prix = (float)m.prix, Image = m.image, Niveau = m.niveau, Notescours = m.notes_cours, Solutionnaires = m.solutionnaire, Laboratoires = m.laboratoires, Exercices = m.exercices, Quiz = m.quiz }).Single<ClsCours>();
            var NbEtudiant = (from m in db.etudiantcours
                              where m.idcours == Int32.Parse(id)
                              select m).Count();
            ViewBag.NbEtudiant = NbEtudiant.ToString();
            ViewBag.liens = getListChapitre(int.Parse(id));
            ViewBag.NbCours = ViewBag.liens.Count;
           
            return View(resultat);
        }
        public ActionResult Chat(string loginreciever)
        {
            Session["lastRecordId"] = 0;
            DataLinkDataContext db = new DataLinkDataContext();
            student s = new student();
            var resultat = (from m in db.student
                            from ec in db.etudiantcours
                            from c in db.cours
                            where c.auteur == int.Parse(Session["IdEnseignant"].ToString())
                            && ec.idcours == c.Idcours
                            && ec.idetudiant==m.Id    
                            select new ClsEtudiant { IdEtudiant = m.Id, LoginEtudiant = m.login }).ToList();
            ViewBag.listLogins = resultat;
            ViewBag.loginreciever = loginreciever;

            return View();
        }
        [HttpPost]
        public ActionResult ChatMessage(string MsgText, int id, string to)
        {
            DataLinkDataContext db = new DataLinkDataContext();
            string loginEnseignant = (string)Session["LoginEnseignant"];
            if (id != 0)
            {
                id = (int)Session["lastRecordMsg"];
            }
            conversations cnv = new conversations
            {

                login_sender = loginEnseignant,
                login_reciever = to,
                date_inscrit = DateTime.Now,
                contenu = MsgText
            };
            if (cnv.contenu != "")
            {
                db.conversations.InsertOnSubmit(cnv);
                try
                {
                    db.SubmitChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.viol_constraint = "your are already enrolled,";
                }
            }
            else
            {

                var req1 = from m in db.conversations
                           where
                           m.login_reciever == cnv.login_reciever
                           &&
                           m.login_sender == cnv.login_sender
                           orderby m.Id descending
                           select m;
                var req2 = from m in db.conversations
                           where
                           m.login_sender == cnv.login_reciever
                           &&
                           m.login_reciever == cnv.login_sender
                           orderby m.Id descending
                           select m;
                var req3 = req1.Union(req2);
               
                var resultat = (from m in req3
                                where m.Id > id
                                orderby m.Id ascending
                                select new { contenu = m.contenu, id = m.Id, dateinscrit = m.date_inscrit, login = m.login_sender, loginsender = m.login_sender }).ToList();
                if (resultat.Count() != 0) { Session["lastRecordMsg"] = resultat.Last().id; }
                return Json(resultat);
            }
            ViewBag.idreciever = to;
            return Json("");
        }
        private List<string> getListChapitre(int id)
        {

            DataLinkDataContext db = new DataLinkDataContext();
            cours s1 = new cours();



            var resultat = (from c in db.chapitre
                            where c.id_cours==id
                            select c.lien).ToList();


            return resultat;
        }
    }
}