﻿using projetFinal.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace projetFinal.Controllers
{
    public class HomeController : Controller
    {

        [HttpPost]
        public ActionResult Index(ClsEtudiant E1)
        {
            searchUser ss = new searchUser();
            ClsEtudiant tmpEtudiant = new ClsEtudiant();
            tmpEtudiant = ss.matchStudent(E1);
            returnCategories();

            if (tmpEtudiant != null)
            {
                if (tmpEtudiant.Password == E1.Password)
                {
                    switch (tmpEtudiant.Status)
                    {
                        case 0:
                            return View("changePassword");

                        case 1:
                            Session["IdEtudiant"] = tmpEtudiant.IdEtudiant.ToString();
                            Session["LoginEtudiant"] = tmpEtudiant.LoginEtudiant.ToString();
                            Session["FullName"] = tmpEtudiant.Nom+" "+ tmpEtudiant.Prenom;
                            Session["niveauEtudiant"] = tmpEtudiant.Niveau.ToString();
                            Session["lastRecordId"] = 0;
                            Session["lastRecordMsg"] = 0;
                            returnCategorieDetails("art");
                            return View("studentCorner", getListCours(tmpEtudiant.Niveau, "art"));

                    }


                }
            }
            DataLinkDataContext db = new DataLinkDataContext();

            var Query = from element in db.categorie
                        select element.nomcat;

            ViewBag.cat = Query;

            IEnumerable<ClsCours> resultat = new List<ClsCours>();
            List<ClsCours> res = new List<ClsCours>();
            foreach (string element in Query)
            {

                resultat = (from Item in db.cours
                            from e in db.enseignants
                            where Item.nomcat == element
                            && e.Id == Item.auteur
                            select new ClsCours
                            {
                                Id = (Item.Idcours).ToString(),
                                Nomcours = Item.nomcours,
                                Description = Item.description,
                                Nbremodule = Item.nbremodule,
                                Auteur = e.nom + " " + e.prenom,
                                Prix = (float)Item.prix,
                                Image = Item.image,
                                Niveau = Item.niveau,
                                Categorie = Item.nomcat
                            }).Take(4).ToList();
                if (resultat != null)
                {
                    foreach (var element4 in resultat)
                    {
                        res.Add(element4);
                    }

                }


            }

            @ViewBag.data = "invalide user or password";
            return View(res);
        }

        public ActionResult Index()
        {
            //affichages des cartes des cours suivants leurs categories 
            DataLinkDataContext db = new DataLinkDataContext();


            var Query = from element in db.categorie
                        select element.nomcat;

            ViewBag.cat = Query;

            IEnumerable<ClsCours> resultat = new List<ClsCours>();
            List<ClsCours> res = new List<ClsCours>();
            foreach (string element in Query)
            {

                resultat = (from Item in db.cours
                            from e in db.enseignants
                            where Item.nomcat == element
                            && e.Id == Item.auteur
                            select new ClsCours
                            {
                                Id = (Item.Idcours).ToString(),
                                Nomcours = Item.nomcours,
                                Description = Item.description,
                                Nbremodule = Item.nbremodule,
                                Auteur = e.nom + " " + e.prenom,
                                Prix = (float)Item.prix,
                                Image = Item.image,
                                Niveau = Item.niveau,
                                Categorie = Item.nomcat
                            }).Take(4).ToList();
                if (resultat != null)
                {
                    foreach (var element4 in resultat)
                    {
                        res.Add(element4);
                    }

                }


            }

            return View(res);
        }
        [HttpPost]
        public ActionResult loginEnseignant(enseignants E1)
        {
            searchUser ss = new searchUser();
            ClsEnseignant tmpEnseignant = new ClsEnseignant();
            tmpEnseignant = ss.matchTeacher(E1);
            returnCategories();

            if (tmpEnseignant != null)
            {
                if (tmpEnseignant.Password == E1.password)
                {
                            Session["IdEnseignant"] = tmpEnseignant.Id.ToString();
                            Session["LoginEnseignant"] = tmpEnseignant.Login.ToString();
                            Session["lastRecordMsg"] = 0;
                            Session["FullName"] = tmpEnseignant.Nom + " " + tmpEnseignant.Prenom;
                    DataLinkDataContext db = new DataLinkDataContext();
                    cours s1 = new cours();
                    var requet = from element in db.niveauscolaire
                                 select element.niveau;


                    ViewBag.niveau = requet;


                    var resultat = (from m in db.cours
                                    where m.auteur == tmpEnseignant.Id
                    select new ClsCours { Id = (m.Idcours).ToString(), Nomcours = m.nomcours, Description = m.description, Nbremodule = m.nbremodule, Prix = (float)m.prix, Image = m.image, Niveau = m.niveau, Categorie = m.nomcat }).ToList();
                    returnCategories();
                    return View("~/Views/Enseignant/TeacherCorner.cshtml", resultat);
                }
            }
            @ViewBag.data = "invalide user or password";
            return View();
        }
        public ActionResult loginEnseignant()
        {
            returnCategories();
            return View();
        }
        public ActionResult inscription()
        {

            DataLinkDataContext nv = new DataLinkDataContext();

            var requet = from element in nv.niveauscolaire
                         select element.niveau;


            ViewBag.niveau = requet;
            returnCategories();
            return View();



        }

        [HttpPost]
        public ActionResult inscription(ClsEtudiant E1)
        {
            DataLinkDataContext db = new DataLinkDataContext();
            returnCategories();
            student s1 = new student();
            s1.nom = E1.Nom;
            s1.niveau = E1.Niveau;
            s1.email = E1.Email;
            s1.login = CreateLogin();
            s1.password = Createpassword();
            s1.status = 0;
            db.student.InsertOnSubmit(s1);
            db.SubmitChanges();
            Email email = new Email();
            email.To = E1.Email;
            email.Subject = "Confirmation login and password";
            email.Message = "Hello,\n\nYour temporary password and username are:\nUSERNAME: " + s1.login + "\nPASSWORD: " + s1.password + "\n\nThank you.";

            if (sendMail(email))
            {
                ViewBag.mailStatus = "message sent";
            }
            else
            {
                ViewBag.mailStatus = "message not sent";
            }
            DataLinkDataContext nv = new DataLinkDataContext();
            var requet = from element in nv.niveauscolaire
                         select element.niveau;
            ViewBag.niveau = requet;
            return View();
        }

        public ActionResult changePassword()
        {
            returnCategories();
            return View();

        }

        [HttpPost]
        public ActionResult changePassword(changePassword changeMe)
        {
            returnCategories();
            if (CheckExitingLogin(changeMe.newLogin))
            {
                @ViewBag.messageFail = "Failed ! Login is already taken!!!";
            }
            else
            {
                DataLinkDataContext db = new DataLinkDataContext();
                student nUser = db.student.Single(u => u.login == changeMe.Login);
                nUser.login = changeMe.newLogin;
                nUser.password = changeMe.newPassword;
                nUser.status = 1;
                db.SubmitChanges();
                @ViewBag.message = "Success Update";

            }


            return View();
        }

        [HttpGet]
        public ActionResult studentCorner(string nomcat)
        {
            Session["lastRecordId"] = 0;
            Session["lastRecordMsg"] = 0;
            if (nomcat != null)
            {
                returnCategories();
                returnCategorieDetails(nomcat);
                return View(getListCours(Session["niveauEtudiant"].ToString(), nomcat));
            }
            returnCategories();
            return View();
        }

        public ActionResult ShowDetailCours(string id)
        {
            Session["lastRecordId"] = 0;
            Session["idCours"] = id;
            DataLinkDataContext db = new DataLinkDataContext();
            cours s1 = new cours();

            returnCategories();

            var resultat = (from m in db.cours
                            from e in db.enseignants
                            where m.Idcours == Int32.Parse(id)
                            && e.Id==m.auteur
                            select new ClsCours { Id = (m.Idcours).ToString(), Nomcours = m.nomcours, Description = m.description, Nbremodule = m.nbremodule, Auteur = e.nom+" "+e.prenom, Prix = (float)m.prix, Image = m.image, Niveau = m.niveau,Notescours = m.notes_cours,Solutionnaires = m.solutionnaire,Laboratoires = m.laboratoires,Exercices = m.exercices,Quiz = m.quiz }).Single<ClsCours>();
            var verify = from m in db.etudiantcours
                         where m.idcours == Int32.Parse(id)
                         && m.idetudiant == Int32.Parse(Session["IdEtudiant"].ToString())
                         select m;
            var NbEtudiant = (from m in db.etudiantcours
                              where m.idcours == Int32.Parse(id)
                              select m).Count();
            ViewBag.NbEtudiant = NbEtudiant.ToString();
            ViewBag.inscrit = false;
            if (verify.Count() != 0)
            {
                ViewBag.inscrit = true;
            }
            ViewBag.liens = getListChapitre(int.Parse(id));
            ViewBag.NbCours = ViewBag.liens.Count;
            return View(resultat);
        }

        public ActionResult InscrireCours(string idcours)
        {
            DataLinkDataContext db = new DataLinkDataContext();
            string idEtudiant = (string)Session["IdEtudiant"];
            returnCategories();

            etudiantcours tmp = new etudiantcours
            {
                idcours = int.Parse(idcours),
                idetudiant = int.Parse(idEtudiant),
                dateinscrite = DateTime.Now

            };
            db.etudiantcours.InsertOnSubmit(tmp);


            try
            {
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                ViewBag.viol_constraint = "your are already enrolled,";

                return View();
            }
            ViewBag.Success = "You can access to the documemtation now";
            return RedirectToAction("ShowDetailCours", new ClsCours { Id = idcours });
        }

        [HttpPost]
        public ActionResult CommentCours(string MsgText, int Idcours, int i)
        {
            DataLinkDataContext db = new DataLinkDataContext();
            returnCategories();
            string idEtudiant = (string)Session["IdEtudiant"];
            string loginEtudiant = (string)Session["LoginEtudiant"];

            commentaires cmt = new commentaires
            {
                Idcours = Idcours,
                Idetudiant = int.Parse(idEtudiant),
                date_commentaire = DateTime.Now,
                contenu = MsgText
            };
            if (cmt.contenu != "")
            {
                db.commentaires.InsertOnSubmit(cmt);
                try
                {
                    db.SubmitChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.viol_constraint = "your are already enrolled";
                }
            }
            else
            {

                var resultat = (from m in db.commentaires
                                from e in db.student
                                where m.Idetudiant == e.Id
                                && m.Idcours == Idcours

                                orderby m.Id descending
                                select new { contenu = m.contenu, id = m.Id, dateinscrit = m.date_commentaire, login = e.login, nom = e.nom, prenom = e.prenom }).Take(i).Skip(0).ToList();
                return Json(resultat);
            }
            return Json(null);
        }

        public ActionResult Chat(string loginteacher)
        {
            returnCategories();
            Session["lastRecordId"] = 0;
            DataLinkDataContext db = new DataLinkDataContext();
            var resultat1 = (from ec in db.etudiantcours
                            from c in db.cours
                            from p in db.enseignants
                            where ec.idetudiant == int.Parse(Session["IdEtudiant"].ToString())
                            && c.Idcours == ec.idcours
                            && p.Id == c.auteur
                            select new ClsEnseignant { Id = p.Id, Login = p.login }).ToList();
            ViewBag.listLoginsEns = resultat1;
            ViewBag.loginreciever = loginteacher;
            
            return View();
        }

        [HttpPost]
        public ActionResult ChatMessageEns(string MsgText,int id,string to)
        {
            DataLinkDataContext db = new DataLinkDataContext();
            returnCategories();
            string LoginEtudiant = (string)Session["LoginEtudiant"];
            if (id != 0)
            {
                id = (int)Session["lastRecordMsg"];
            }
            conversations cnv = new conversations
            {

                login_sender = LoginEtudiant,
                login_reciever = to,
                date_inscrit = DateTime.Now,
                contenu = MsgText
            };
            if (cnv.contenu != "")
            {
                db.conversations.InsertOnSubmit(cnv);
                try
                {
                    db.SubmitChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.viol_constraint = "your are already enrolled,";
                }
            }
            else
            {

                var req1 = from m in db.conversations
                           where
                           m.login_reciever == cnv.login_reciever
                           &&
                           m.login_sender == cnv.login_sender
                           orderby m.Id descending
                           select m;
                var req2 = from m in db.conversations
                           where
                           m.login_sender == cnv.login_reciever
                           &&
                           m.login_reciever == cnv.login_sender
                           orderby m.Id descending
                           select m;
                var req3 = req1.Union(req2);
                var resultat = (from m in req3
                                where m.Id > id
                                orderby m.Id ascending
                                select new { contenu = m.contenu, id = m.Id, dateinscrit = m.date_inscrit, login = m.login_sender, loginsender = m.login_sender }).ToList();
                if (resultat.Count() != 0) { Session["lastRecordMsg"] = resultat.Last().id; }
                return Json(resultat);
            }
            ViewBag.idreciever = to;
            return Json("");
        }

        public ActionResult MesCours()
        {
            returnCategories();

            DataLinkDataContext db = new DataLinkDataContext();
            cours s1 = new cours();
            string idEtudiant = (string)Session["IdEtudiant"];
            string niveauEtudiant = (string)Session["niveauEtudiant"];



            var resultat = (from m in db.cours
                            from e in db.etudiantcours
                            from t in db.enseignants
                            where m.niveau == niveauEtudiant
                            && e.idcours==m.Idcours
                            && t.Id==m.auteur
                            && e.idetudiant== int.Parse(idEtudiant)

                            //select new ClsCours { Nomcours = m.nomcours, Description = m.description, Nbremodule = m.nbremodule, Auteur = m.auteur, Prix = (float)m.prix, Image = m.image, Niveau = m.niveau }).Single<ClsCours>();
                            select new ClsCours { Id = (m.Idcours).ToString(), Nomcours = m.nomcours, Description = m.description, Nbremodule = m.nbremodule, Auteur = t.nom+" "+t.prenom, Prix = (float)m.prix, Image = m.image, Niveau = m.niveau, Categorie = m.nomcat }).ToList();



            return View(resultat);
        }


        public ActionResult About()
        {
            returnCategories();

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        //Guest Layout
        public ActionResult _Layout()
        {
            return View();
        }
        //Student Layout
        public ActionResult _LayoutEtudiant()
        {
            Session["lastRecordId"] = 0;
            Session["lastRecordMsg"] = 0;
            return View();
        }
       
        
        
        
        
        
        // Les methodes
        private bool sendMail(Email email)
        {
            try
            {
                string MailSender = System.Configuration.ConfigurationManager.AppSettings["MailSender"].ToString();
                string MailPw = System.Configuration.ConfigurationManager.AppSettings["MailPw"].ToString();

                SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);
                smtpClient.EnableSsl = true;
                smtpClient.Timeout = 100000;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.UseDefaultCredentials = false;
               
                smtpClient.Credentials = new System.Net.NetworkCredential(MailSender, MailPw);

                MailMessage mailMessage = new MailMessage(MailSender, email.To, email.Subject, email.Message);
                // mailMessage.IsBodyHtml = true;
                mailMessage.BodyEncoding = System.Text.UTF8Encoding.UTF8;

                smtpClient.Send(mailMessage);

                return true;
            }
            catch (Exception ex)
            {
                return true;
            }
        }
        
        private string Createpassword()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[8];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);


            return finalString;
        }

        private string CreateLogin()
        {
            DataLinkDataContext db = new DataLinkDataContext();
            bool test = true;
            var finalLogin = "";
            var loginFromDb = from element in db.student
                              select element.login;
            while (test)
            {
                var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                var login = new char[8];
                var random = new Random();

                for (int i = 0; i < login.Length; i++)
                {
                    login[i] = chars[random.Next(chars.Length)];
                }

                finalLogin = new String(login);
                foreach (string element in loginFromDb)
                {
                    if (element == finalLogin)
                    { test = false; }


                }

                if (test)
                { test = false; }
                else { test = true; }



            }


            return finalLogin;
        }
        
        private bool CheckExitingLogin( string userlogin)
        {
            DataLinkDataContext db = new DataLinkDataContext();
            bool test = false;
           
            var loginFromDb = from element in db.student
                              select element.login;
           
                foreach (string element in loginFromDb)
                {
                    if (element == userlogin)
                    { test = true; }


                }

          return test;
        }
        
        private List<ClsCours> getListCours(string niveau,string nomCat)
        {

            //var EtudiantTarget = Session["EtudiantTarget"] as ClsEtudiant;
            // List<cours> listeCours = new List<cours>();
            DataLinkDataContext db = new DataLinkDataContext();
            cours s1 = new cours();



            var resultat = (from m in db.cours
                            from e in db.enseignants
                            where m.niveau == niveau
                            && m.nomcat== nomCat
                            && e.Id== m.auteur
                            //select new ClsCours { Nomcours = m.nomcours, Description = m.description, Nbremodule = m.nbremodule, Auteur = m.auteur, Prix = (float)m.prix, Image = m.image, Niveau = m.niveau }).Single<ClsCours>();
                            select new ClsCours {Id = (m.Idcours).ToString(), Nomcours = m.nomcours, Description = m.description, Nbremodule = m.nbremodule, Auteur = e.nom+" "+e.prenom, Prix = (float)m.prix, Image = m.image, Niveau = m.niveau , Categorie=m.nomcat}).ToList();


            return resultat;


        }

        private void returnCategories()
        {
            DataLinkDataContext db = new DataLinkDataContext();

            var Query = from element in db.categorie
                        select element.nomcat;

            ViewBag.cat = Query;
        }

        private void returnCategorieDetails(string NomCat)
        {
            DataLinkDataContext db = new DataLinkDataContext();

            var Query = (from element in db.categorie
                         where element.nomcat == NomCat
                         select new ClsCategorie { Nom = element.nomcat, Desc = element.description_cat, Img = element.image }).Single();

            ViewBag.catNom = Query.Nom;
            ViewBag.catDesc = Query.Desc;
            ViewBag.catImg = Query.Img;
        }
        private List<string> getListChapitre(int id)
        {

            DataLinkDataContext db = new DataLinkDataContext();
            cours s1 = new cours();



            var resultat = (from c in db.chapitre
                            where c.id_cours == id
                            select c.lien).ToList();


            return resultat;
        }
    }
}
