﻿using projetFinal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace projetFinal
{
    public class searchUser
    {

        //public string searchStudent(ClsEtudiant E1)
        //{

        //    DataLinkDataContext db = new DataLinkDataContext();
        //    student s1 = new student();
        //    string passout = "";
        //    // var pass = from m in db.registers where m.emailid == li.Emailid select m.userpassword;  
        //    var pass = from m in db.student where m.login == E1.Login select m.password ;
        //    foreach (string query in pass)
        //    {
        //        passout = query;

        //    }
        //    return passout;

        //}
        //public int searchStatus(ClsEtudiant E1)
        //{

        //    DataLinkDataContext db = new DataLinkDataContext();
        //    student s1 = new student();
        //    int passout = 0;
        //    // var pass = from m in db.registers where m.emailid == li.Emailid select m.userpassword;  
        //    var pass = from m in db.student where m.login == E1.Login select m.status;
        //    foreach (int query in pass)
        //    {
        //        passout = query;

        //    }
        //    return passout;

        //}

        public ClsEtudiant matchStudent(ClsEtudiant E1)
        {
            try
            {
                ClsEtudiant wantedStudent = new ClsEtudiant();
                DataLinkDataContext db = new DataLinkDataContext();
                student s1 = new student();
                var result = (from m in db.student
                              where m.login == E1.LoginEtudiant
                              select new ClsEtudiant { IdEtudiant = m.Id ,LoginEtudiant = m.login, Password = m.password,Nom = m.nom,Prenom = m.prenom, Niveau = m.niveau, Status = (int)(m.status) }).ToList();

                wantedStudent = result[0];
                return wantedStudent;
            }
            catch (Exception ex)
            {
                return null;
            }

        }
        public ClsEnseignant matchTeacher(enseignants E1)
        {
            try
            {
                DataLinkDataContext db = new DataLinkDataContext();
                var result = (from m in db.enseignants
                              where m.login == E1.login
                              select new ClsEnseignant { Id = m.Id, Nom = m.nom, Prenom = m.prenom, Login = m.login, Password = m.password }).Single();

                
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }

        }


    }
}